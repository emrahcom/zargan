#!/usr/bin/python
#-*- coding:utf-8 -*-

###############################################################################
# file      : zargan.py
# author    : emrah
# usage     : zargan.py [word]
#
# This script is a console based Zargan interface. Zargan is a Turkish-English
# dictionary web site.
###############################################################################

import re
import sys
import httplib
import urllib
import socket

SERVER = "www.zargan.com"
PAGE = "/sozluk.asp"
PATTERN = '<span class="dblClickSpan".*?>(.+?)</span>.*?'
PATTERN += '<span class="dblClickSpan".*?>(.+?)</span>(.*)'
PATTERN = re.compile(PATTERN, re.DOTALL)
COLORED = True
DEBUG = 0



# -----------------------------------------------------------------------------
def get_response_from_zargan(word):
    try:
        header = { 'Content-type': 'application/x-www-form-urlencoded',
                   'Accept': 'text:plain' }
        param = urllib.urlencode({'Sozcuk': word})
        cnn = httplib.HTTPConnection(SERVER)
        socket.setdefaulttimeout(5)
        cnn.request('GET', PAGE, param, header)
        res = cnn.getresponse()
        res = res.read().decode('iso-8859-9')
        cnn.close()

        result = res
    except Exception, err:
        if DEBUG:
            raise
        else:
            print(str(err))

        result = None

    return result



# -----------------------------------------------------------------------------
def parse_response(raw):
    try:
        if not raw:
            raise NameError('No response')

        parsed = []
        while True:
            g = PATTERN.search(raw)
            if not g:
                break
            parsed.append((g.group(1).strip(), g.group(2).strip()))
            raw = g.group(3)

        result = parsed
    except Exception, err:
        if DEBUG:
            raise
        else:
            print(str(err))

        result = None

    return result



# -----------------------------------------------------------------------------
def print_response(parsed):
    try:
        for key, value in parsed:
            if COLORED:
                out =  "\033[35m%s\033[0m : " % (key.strip().encode('utf-8'))
                out += "%s" % (value.strip().encode('utf-8'))
            else:
                out =  "%s : " % (key.strip().encode('utf-8'))
                out += "%s"    % (value.strip().encode('utf-8'))
            print(out)

        result = True
    except Exception, err:
        if DEBUG:
            raise
        else:
            print(str(err))

        result = False

    return result




# -----------------------------------------------------------------------------
if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: %s word [word...]' % sys.argv[0])
        sys.exit(1)

    word = ' '.join(sys.argv[1:])
    raw = get_response_from_zargan(word)
    parsed = parse_response(raw)
    if parsed:
        print_response(parsed)
    else:
        print(":(")

    sys.exit(0)
